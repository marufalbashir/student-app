import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Students } from './students.model';

@Injectable({
  providedIn: 'root'
})
export class RestApiService {
  apiUrl= 'http://localhost:3000';

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  constructor(private http: HttpClient) { }

  getStudents(): Observable<Students> {
    return this.http.get<Students>(this.apiUrl + '/students');
  }

  createStudent(student): Observable<Students> {
    return this.http.post<Students>(this.apiUrl + '/students', JSON.stringify(student), this.httpOptions);
  }
  getStudent(id): Observable<Students> {
    return this.http.get<Students>(this.apiUrl + '/students/' + id);
  }

  updateStudent(id, student): Observable<Students> {
    return this.http.put<Students>(this.apiUrl + '/students/' + id,
    JSON.stringify(student), this.httpOptions);
  }
  
  deleteStudent(id) {
    return this.http.delete<Students>(this.apiUrl + '/students/' + id);
  }
  
}
