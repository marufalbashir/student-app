import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }

  getUserDetails(username,password){
    return this.http.post('http://loacalhost:3000/users',{
      username,
      password
    }).subscribe(data=>{(
      console.log(data,'connection ok')
    )})
  }
}
