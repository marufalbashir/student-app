import { Component, OnInit } from '@angular/core';
import { RestApiService } from '../shared/rest-api.service';

@Component({
  selector: 'app-student-list',
  templateUrl: './student-list.component.html',

  styleUrls: ['./student-list.component.css']
})
export class StudentListComponent implements OnInit {
  Student: any =[];
  
  constructor(public restApi : RestApiService) { }

  ngOnInit() {
    this.loadStudents();
    
  }
  loadStudents(){
    return this.restApi.getStudents().subscribe(data=>this.Student=data);
    
  }

  deleteStudent(id) {
    if (window.confirm('Are you sure you want to delete?')) {
      this.restApi.deleteStudent(id)
        .subscribe(data => this.loadStudents())
    }
  }

}
