/*import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-student-edit',
  templateUrl: './student-edit.component.html',
  styleUrls: ['./student-edit.component.css']
})
export class StudentEditComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
*/

import { Component, OnInit } from '@angular/core';
import { RestApiService } from '../shared/rest-api.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-student-edit',
  templateUrl: './student-edit.component.html',
  styleUrls: ['./student-edit.component.css']
})
export class StudentEditComponent implements OnInit {
  id = this.actRoute.snapshot.params['id'];
  studentData: any = {};

  constructor(
    public restApi: RestApiService,
    public actRoute: ActivatedRoute,
    public router: Router
    ) { }

  ngOnInit() {
    
    this.restApi.getStudent(this.id)
      .subscribe(data => this.studentData = data);
  }

  updateStudent() {
    if(window.confirm('Are you sure you want to update?')) {
      this.restApi.updateStudent(this.id, this.studentData)
        .subscribe(data => {
          this.router.navigate(['/student-list'])
        })
    }
  }

}
