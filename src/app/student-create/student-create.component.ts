import { Component, OnInit, Input } from '@angular/core';
import { RestApiService } from '../shared/rest-api.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-student-create',
  templateUrl: './student-create.component.html',
  styleUrls: ['./student-create.component.css']
})
export class StudentCreateComponent implements OnInit {

  @Input() studentDetails = { name: '', email: '', class: 0,gender:'',mark:'',address:''}

  constructor(public restApi: RestApiService, public router: Router) { }
  ngOnInit() {
  }


  addStudent(dataStudent) {
    this.restApi.createStudent(this.studentDetails)
      .subscribe((data: {}) => {
        this.router.navigate(['/student-list'])
      })
  }
  
}
