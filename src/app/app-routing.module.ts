import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StudentCreateComponent } from './student-create/student-create.component';
import { StudentListComponent } from './student-list/student-list.component';
import { StudentEditComponent } from './student-edit/student-edit.component';
import { LoginComponent } from './login/login.component';

const routes: Routes = [
  {path:'',pathMatch: 'full', redirectTo: 'student-list'},
  {path:'create-student', component: StudentCreateComponent},
  {path:'student-list', component: StudentListComponent},
  {path:'student-edit/:id', component: StudentEditComponent},
  {path:'login', component: LoginComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { } 
